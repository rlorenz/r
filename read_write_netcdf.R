#-----------------------------------------------------------------------------#
# File Name: read_write_netcdf.R
# Creation Date: 12 October 2017
# Last Modified:
# Created by: Ruth Lorenz
# Purpose: read data from netcdf, write data to netcdf example script

#-----------------------------------------------------------------------------#
# Load libraries
#-----------------------------------------------------------------------------#
library(ncdf4)

#-----------------------------------------------------------------------------#
# Define user parameters
#-----------------------------------------------------------------------------#
# define directory where input data is
indir <- "/net/tropo/climphys/rlorenz/E-Obs_Textremes/"
# define output directory  (here the same) 
outdir <- "/net/tropo/climphys/rlorenz/E-Obs_Textremes/"

#-----------------------------------------------------------------------------#
# Read data
#-----------------------------------------------------------------------------#
file <- paste(indir, "tx_monmax_0.25deg_reg_v15.0.nc", sep="")
ex.nc <- nc_open(file)
print(ex.nc)

# Read variable "tx"
tx <- ncvar_get(ex.nc, "tx")

# Read longitude and latitude dimension
lon <- ncvar_get(ex.nc, "longitude")
lat <- ncvar_get(ex.nc, "latitude")
nlon <- dim(lon)                 #number of longitudes, do only if needed
nlat <- dim(lat)                 #number of latitudes, 

#Read time dimension
time <- ncvar_get(ex.nc, "time")
time_units <- ncatt_get(ex.nc, "time", "units") #time unit, for how defined
# extract base date, third bit of time_units "days since 1950-01-01 00:00"
base_date <- as.character(lapply(strsplit(as.character(time_units$value),
                                          split = " "), "[", 3))
time_d <- as.Date(time, format = "%j",
                  origin = as.Date(base_date)) # define time as date
time.y <- format(time_d, "%Y") #extract years only from time_d

Y.Start <- time.y[1] # first year in timeseries
                     # -> can be used as info for output file
Y.End <- time.y[length(time.y)] # last year in timeseries

#-----------------------------------------------------------------------------#
# Perform calculations, none in this example script
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# save as NetCDF output
#-----------------------------------------------------------------------------#
print("save data to netcdf")

#define dimensions for output file
dim1 <- ncdim_def("longitude", "degrees_east", as.double(lon))
dim2 <- ncdim_def("latitude", "degrees_north", as.double(lat))
dimT <- ncdim_def("time", time_units$value, time, unlim = FALSE)
missval <- -9999

# define the EMPTY netcdf variable "TX"
var1 <- ncvar_def("tx", "degC", list(dim1, dim2, dimT), missval,
                  longname = "maximum temperature")

# associate the netcdf variable with a netcdf file  
file.out <- paste(outdir, "TX_", Y.Start, "-", Y.End, ".nc", sep = "")
print(file.out)
# in case the output directory does not exist yet, create it
if (!file.exists(outdir)){
   dir.create(file.path(outdir), showWarnings = FALSE)
}

# create the netcdf file
nc.ex = nc_create(file.out, list(var1))

# put data into file
ncvar_put(nc.ex, var1, tx) # one could write a subset of the data using
                           # start = c(1, 1, 1),  count = c(nlon, nlat, t2))
ncatt_put(nc.ex , var1, '_FillValue', missval )

# close the file
nc_close(nc.ex)
